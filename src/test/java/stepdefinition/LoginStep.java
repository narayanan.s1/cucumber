package stepdefinition;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginStep {
	static WebDriver driver=null;
	@Given("Open the webpage")
	public void open_the_webpage()
	{
	driver = new ChromeDriver();
	driver.get("https://demowebshop.tricentis.com/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
	}
	@Then("the home page of website get displayed")
	public void the_home_page_of_website_get_displayed()
	{
	System.out.println("Home page displayed");	
	}
	@When("enters the valid email in emailfield")
	public void enters_the_valid_email_in_emailfield()
	{
		driver.findElement(By.linkText("Log in")).click();
		driver.findElement(By.id("Email")).sendKeys("narayanansagar23@gmail.com");
	}
	@When("enter the valid password in passwordfield")
	public void enter_the_valid_password_in_passwordfield()
	{
		driver.findElement(By.id("Password")).sendKeys("Pranav@32");
	}
	@Then("click on login button")
	public void click_on_login_button()
	{
		driver.findElement(By.xpath("//div[@class='buttons']//input[@type='submit']")).click();
	}
	@Then("the home page of the loged user name shold be displayed")
	public void the_home_page_of_the_loged_user_name_shold_be_displayed()
	{
		System.out.println("loged user name displayed");
	}
	@Then("click on the logout")
	public void click_on_the_logout()
	{
		driver.findElement(By.linkText("Log out")).click();
	}
	@Then("the user shold be taken to the home page of the login")
	public void the_user_shold_be_taken_to_the_home_page_of_the_login()
	{
		System.out.println("login page display");
	}
	@Then("close the browser")
	public void close_the_browser()
	{
		driver.quit();
	}
	
}
